package cz.educanet.galacticprojekt;

import javax.swing.*;
import java.awt.*;

public class HlavniOkno extends JFrame {
 public void nastavOkno(){
  setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
  setTitle("galactic projekt");
  setSize(605, 450);
  setLocationRelativeTo(null);
  Container plocha = getContentPane();
  plocha.setLayout(null);


  JLabel labNeco = new JLabel();
  labNeco.setSize(50, 50);
  labNeco.setLocation(50, 50);
  plocha.add(labNeco);
  labNeco.setIcon(new ImageIcon(getClass().getResource("/cz/educanet/galacticprojekt/img/ektor.jpg")));
 }
}
