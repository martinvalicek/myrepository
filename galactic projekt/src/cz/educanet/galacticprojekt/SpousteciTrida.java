package cz.educanet.galacticprojekt;

import javax.swing.*;

public class SpousteciTrida {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(SpousteciTrida::otevriOkno);
    }

    private static void otevriOkno() {
        HlavniOkno okno = new HlavniOkno();
        okno.nastavOkno();
        okno.setVisible(true);
    }
}
