import java.awt.*;
import javax.swing.*;
/*
 * Created by JFormDesigner on Mon Feb 10 08:28:49 CET 2020
 */



/**
 * @author Martin Val쎭쒍ek
 */
public class MainWindow extends JFrame {
    public MainWindow() {
        initComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Martin Val쎭쒍ek
        panel1 = new JPanel();
        zadaniCisel = new JLabel();
        txtField1 = new JTextField();
        txtField2 = new JTextField();
        panel3 = new JPanel();
        scrollPane1 = new JScrollPane();
        txtVysledek = new JTextArea();
        panel2 = new JPanel();
        operace = new JLabel();
        btnPlus = new JButton();
        btnMinus = new JButton();
        btnKrat = new JButton();

        //======== this ========
        setTitle("Kalkulacka");
        setIconImage(new ImageIcon("C:\\Users\\glitz\\Documents\\myrepository\\Kalkulacka\\icon.png").getImage());
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== panel1 ========
        {
            panel1.setBorder (new javax. swing. border. CompoundBorder( new javax .swing .border .TitledBorder (new javax. swing. border.
            EmptyBorder( 0, 0, 0, 0) , "JF\u006frmD\u0065sig\u006eer \u0045val\u0075ati\u006fn", javax. swing. border. TitledBorder. CENTER, javax. swing
            . border. TitledBorder. BOTTOM, new java .awt .Font ("Dia\u006cog" ,java .awt .Font .BOLD ,12 ),
            java. awt. Color. red) ,panel1. getBorder( )) ); panel1. addPropertyChangeListener (new java. beans. PropertyChangeListener( )
            { @Override public void propertyChange (java .beans .PropertyChangeEvent e) {if ("\u0062ord\u0065r" .equals (e .getPropertyName () ))
            throw new RuntimeException( ); }} );
            panel1.setLayout(new FlowLayout());

            //---- zadaniCisel ----
            zadaniCisel.setText("Zadej cisla:");
            zadaniCisel.setFont(new Font("Arial Black", Font.PLAIN, 15));
            panel1.add(zadaniCisel);

            //---- txtField1 ----
            txtField1.setText("Zadej cislo");
            panel1.add(txtField1);

            //---- txtField2 ----
            txtField2.setText("Zadej cislo");
            panel1.add(txtField2);
        }
        contentPane.add(panel1, BorderLayout.NORTH);

        //======== panel3 ========
        {
            panel3.setLayout(new FlowLayout());

            //======== scrollPane1 ========
            {

                //---- txtVysledek ----
                txtVysledek.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
                scrollPane1.setViewportView(txtVysledek);
            }
            panel3.add(scrollPane1);
        }
        contentPane.add(panel3, BorderLayout.SOUTH);

        //======== panel2 ========
        {
            panel2.setLayout(new FlowLayout());

            //---- operace ----
            operace.setText("Vyber si operaci:");
            operace.setFont(new Font("Arial Black", Font.PLAIN, 15));
            panel2.add(operace);

            //---- btnPlus ----
            btnPlus.setText("+");
            btnPlus.setFont(new Font("Arial Black", Font.PLAIN, 12));
            panel2.add(btnPlus);

            //---- btnMinus ----
            btnMinus.setText("-");
            btnMinus.setFont(new Font("Arial Black", Font.PLAIN, 12));
            panel2.add(btnMinus);

            //---- btnKrat ----
            btnKrat.setText("*");
            btnKrat.setFont(new Font("Arial Black", Font.PLAIN, 12));
            panel2.add(btnKrat);
        }
        contentPane.add(panel2, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Martin Val쎭쒍ek
    private JPanel panel1;
    private JLabel zadaniCisel;
    private JTextField txtField1;
    private JTextField txtField2;
    private JPanel panel3;
    private JScrollPane scrollPane1;
    private JTextArea txtVysledek;
    private JPanel panel2;
    private JLabel operace;
    private JButton btnPlus;
    private JButton btnMinus;
    private JButton btnKrat;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
