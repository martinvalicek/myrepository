import javax.swing.*;
import java.net.URISyntaxException;

public class StartClass {
    public static void main(String[] args) throws URISyntaxException {
        StartClass.MainWindow();
    }

    private static void MainWindow() throws URISyntaxException {
        new MainWindow().setVisible(true);
    }
}