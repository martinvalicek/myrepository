package one.hanka;

import javax.swing.*;
import java.awt.*;
import java.net.URI;
import java.net.URISyntaxException;

public class HlavniOkno  extends JFrame {
    private JLabel labEvidence = new JLabel();
    private JLabel labMotorky = new JLabel();
    private JLabel labPalivo = new JLabel();
    private JLabel labAuta = new JLabel();
    private JLabel labSpotreba = new JLabel();

    private JButton btnNeco = new JButton("dsad");

    public void initComponents() throws URISyntaxException {

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sberatel aut");
        setIconImage(Toolkit.getDefaultToolkit().createImage("rgb.png"));
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        setSize(605, 450);
        setLocationRelativeTo(null);

        // nastavení komponent
        {
            contentPane.add(labEvidence);
            labEvidence.setBounds(20, 20, 370, 20);
            labEvidence.setText("Evidence aut a motorek");
            labEvidence.setFont(new Font("Monotype Corsiva", Font.BOLD, 22));

            contentPane.add(labAuta);
            labAuta.setBounds(20, 50, 100, 20);
            labAuta.setText("Auta");

            contentPane.add(labMotorky);
            labMotorky.setText("Motorky");
            labMotorky.setFont(new Font("Segoe UI", Font.PLAIN, 18));

            contentPane.add(labPalivo);
            labPalivo.setBounds(20, 285, 150, 20);
            labPalivo.setText("Celková spotřeba");

            JLabel labPocetAut = new JLabel();
            labPocetAut.setText("Počet aut");

            JLabel labSpotrebaAut = new JLabel();
            labSpotrebaAut.setText("Průmerná spotřeba");
            labSpotrebaAut.setBounds(20, 120, 150, 20);

            JButton btnVypocti = new JButton();
            contentPane.add(btnVypocti);
            btnVypocti.setBounds(20, 150, 545, btnVypocti.getPreferredSize().height);
        }
        // vypočet preferované velikosti
        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
     /* konec přednastavených komponent */

        labEvidence.setText("Evidence aut a motorek");
        contentPane.add(labEvidence);
        labEvidence.setBounds(20, 90, 300, 20);

        labAuta.setText("Pocet aut");
        contentPane.add(labAuta);
        labAuta.setBounds(20, 115, 300, 20);

        labPalivo.setText("Spotreba paliva");
        contentPane.add(labPalivo);
        labPalivo.setBounds(20, 130, 300, 20);

        labMotorky.setText("Pocet motorek");
        contentPane.add(labMotorky);
        labMotorky.setBounds(400, 115, 300, 20);

        labSpotreba.setText("Prumerna spotreba");
        contentPane.add(labSpotreba);
        labMotorky.setBounds(400, 130, 300, 20);

        contentPane.add(btnNeco);

        //TODO: Dopiš metodu po kliknutí dle sceenshotu aplikace. Celá stáj najede za den průměrně 98 km denně (každé vozidlo). Pneu měníme každý rok.
    }
}
