import java.awt.*;
import java.util.Scanner;

public class DatTypy {
    public static void main(String[] args) {
        String s = "Krokonosohroch";

        System.out.println(s.startsWith("Krok"));

        System.out.println(s.contains("nos"));

        String s2 = "Python je nejlepší";
        String s3 = s2.replace("Python", "Java");

        System.out.println(s3);

        s3=s3.trim();

        System.out.println(s3);

        int volba;
        Scanner skenr = new Scanner(System.in, "UTF-8");

        volba = skenr.nextInt();

        switch (volba) {
            case 1: System.out.println(s.startsWith("Krok"));
                  break;
            case 2: System.out.println(s.endsWith("hroch"));
                  break;
            case 3: System.out.println(s.contains("roh"));
                  break;
            case 4: System.out.println("Have a nice day");
                  break;
        }

    }
}
