/**
 *
 * Start class je startovací třída která zároven inicializuje (startuje) třídu MainWindow.
 *
 */
public class StartClass {


    public static void main(String[] args) {
        StartClass.mainWindow();
    }

    private static void mainWindow() {
        new Kalkulacka().setVisible(true);
    }

    /*

    To co jsi zde vytvořil se říká metoda třídy. Tady konkrétně je to statická metoda třídy StartClass.
    Nicméně metody se vždy píší malými písmeny např.

    mainWindow
    addListener
    removeUser
    createUser
    setNumber
    apod.

    private static void MainWindow() {
        new Kalkulacka().setVisible(true);
    }*/

}