import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.plaf.*;

/**
 * @author Martin Val쎭쒍ek
 */
public class Kalkulacka extends JFrame {

    /**
     * Konstruktor třídy Kalkulačka
     */
    public Kalkulacka() {
        initComponents();
    }

    private void btnCislo1ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "1");
    }

    private void btnCislo2ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "2");
    }

    private void btnCislo3ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "3");
    }

    private void btnCislo4ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "4");
    }

    private void btnCislo5ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "5");
    }

    private void btnCislo6ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "6");
    }

    private void btnCislo7ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "7");
    }

    private void btnCislo8ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "8");
    }

    private void btnCislo9ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "9");
    }

    private void btnCislo0ActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "0");
    }

    private void btnPlusActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "+");
    }

    private void btnKratActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "*");
    }

    private void btnMinusActionPerformed(ActionEvent e) {
        txtVysledek.setText(txtVysledek.getText() + "-");
    }

    /**
     * Zpracovani vysledku je trochu slozitejsi nez se zda. Ale i tak dokaze spracovat jen 2 cisla napr 1+1 nebo 4332*2131
     */
    private void btnVysledekAction(final ActionEvent e) {
        //Vezme text z aktualního nastaveni lable
        String actualText = txtVysledek.getText();
        //Rozdeli tento text podle -+*/
        String[] numbers = actualText.split("[-+*/]");
        System.out.println(numbers[0] + " " + numbers[1]);

        //nastavi cislo na 0
        int fistNumber = 0;
        //check jestli se jedna o cislo regexem.
        if(isNumeric(numbers[0])) {
            fistNumber = Integer.parseInt(numbers[0]);
        }
        //nastavi druhe cislo na 0
        int secondNumber = 0;
        if(isNumeric(numbers[1])) {
            secondNumber = Integer.parseInt(numbers[1]);
        }
        //Podle toho jake znamenko obsahuje lable tak to udela operaci -> velice primitivni
        if(actualText.contains("+")) {
            txtVysledek.setText(String.valueOf(fistNumber + secondNumber));
        }

        if(actualText.contains("-")) {
            txtVysledek.setText(String.valueOf(fistNumber - secondNumber));
        }

        if(actualText.contains("*")) {
            txtVysledek.setText(String.valueOf(fistNumber * secondNumber));
        }

    }

    //Pattern je regex -> slouzi to definovani textu a parsovani.
    private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    /**
     * Check jestli je to cislo
     * @param strNum
     * @return
     */
    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }

    /**
     * Celu metodu init components jsem přemístil hned pod konstruktor Kalkulacka() a to z toho důvodu aby se to lépe četlo.
     */
    private void initComponents() {
        //GEN-BEGIN:initComponents
        results = new JPanel();
        txtVysledek = new JLabel();
        calculatorButtons = new JPanel();
        btnRovnost = new JButton();
        clean = new JButton();
        numbers = new JPanel();
        btnCislo1 = new JButton();
        btnCislo2 = new JButton();
        btnCislo3 = new JButton();
        btnCislo4 = new JButton();
        btnCislo5 = new JButton();
        btnCislo6 = new JButton();
        btnCislo7 = new JButton();
        btnCislo8 = new JButton();
        btnCislo9 = new JButton();
        btnCislo0 = new JButton();
        operators = new JPanel();
        btnPlus = new JButton();
        btnMinus = new JButton();
        btnKrat = new JButton();

        //======== this ========
        setTitle("Kalkulacka");
        //vzdy nastav relativni cestu jinak budes mit problem na jinych PC to rozjet
        setIconImage(new ImageIcon("icon.png").getImage());
        var contentPane = getContentPane();

        // Nastavil jsem jednoduche layouty pak se vyhnes problematickym generovanym hodnotam, kterym nikdo nerozumi
        contentPane.setLayout(new BorderLayout());

        //======== results ========
        {
            results.setLayout(new BorderLayout());

            //---- txtVysledek ----
            txtVysledek.setFont(new Font("Arial", Font.BOLD, 30));
            txtVysledek.setPreferredSize(new Dimension(1000, 40));
            txtVysledek.setHorizontalAlignment(SwingConstants.CENTER);
            results.add(txtVysledek, BorderLayout.CENTER);
        }
        contentPane.add(results, BorderLayout.NORTH);

        //======== calculatorButtons ========
        {
            calculatorButtons.setBackground(null);
            calculatorButtons.setLayout(new BorderLayout());

            //---- btnRovnost ----
            btnRovnost.setText("=");
            btnRovnost.setFont(new Font("Segoe UI", Font.PLAIN, 20));
            btnRovnost.addActionListener(e -> btnVysledekAction(e));
            calculatorButtons.add(btnRovnost, BorderLayout.SOUTH);

            //---- clean ----
            clean.setText("C");
            calculatorButtons.add(clean, BorderLayout.WEST);
            clean.addActionListener(e -> btnClean(e));

            //======== numbers ========
            {
                numbers.setLayout(new FlowLayout());

                //---- btnCislo1 ----
                btnCislo1.setText("1");
                btnCislo1.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo1.addActionListener(e -> btnCislo1ActionPerformed(e));
                numbers.add(btnCislo1);

                //---- btnCislo2 ----
                btnCislo2.setText("2");
                btnCislo2.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo2.addActionListener(e -> btnCislo2ActionPerformed(e));
                numbers.add(btnCislo2);

                //---- btnCislo3 ----
                btnCislo3.setText("3");
                btnCislo3.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo3.addActionListener(e -> btnCislo3ActionPerformed(e));
                numbers.add(btnCislo3);

                //---- btnCislo4 ----
                btnCislo4.setText("4");
                btnCislo4.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo4.addActionListener(e -> btnCislo4ActionPerformed(e));
                numbers.add(btnCislo4);

                //---- btnCislo5 ----
                btnCislo5.setText("5");
                btnCislo5.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo5.addActionListener(e -> btnCislo5ActionPerformed(e));
                numbers.add(btnCislo5);

                //---- btnCislo6 ----
                btnCislo6.setText("6");
                btnCislo6.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo6.addActionListener(e -> btnCislo6ActionPerformed(e));
                numbers.add(btnCislo6);

                //---- btnCislo7 ----
                btnCislo7.setText("7");
                btnCislo7.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo7.addActionListener(e -> btnCislo7ActionPerformed(e));
                numbers.add(btnCislo7);

                //---- btnCislo8 ----
                btnCislo8.setText("8");
                btnCislo8.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo8.addActionListener(e -> btnCislo8ActionPerformed(e));
                numbers.add(btnCislo8);

                //---- btnCislo9 ----
                btnCislo9.setText("9");
                btnCislo9.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo9.addActionListener(e -> btnCislo9ActionPerformed(e));
                numbers.add(btnCislo9);

                //---- btnCislo0 ----
                btnCislo0.setText("0");
                btnCislo0.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnCislo0.addActionListener(e -> btnCislo0ActionPerformed(e));
                numbers.add(btnCislo0);
            }
            calculatorButtons.add(numbers, BorderLayout.CENTER);

            //======== operators ========
            {
                operators.setLayout(new FlowLayout());

                //---- btnPlus ----
                btnPlus.setText("+");
                btnPlus.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnPlus.addActionListener(e -> btnPlusActionPerformed(e));
                operators.add(btnPlus);

                //---- btnMinus ----
                btnMinus.setText("-");
                btnMinus.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnMinus.addActionListener(e -> btnMinusActionPerformed(e));
                operators.add(btnMinus);

                //---- btnKrat ----
                btnKrat.setText("*");
                btnKrat.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                btnKrat.addActionListener(e -> btnKratActionPerformed(e));
                operators.add(btnKrat);
            }
            calculatorButtons.add(operators, BorderLayout.NORTH);
        }
        contentPane.add(calculatorButtons, BorderLayout.CENTER);
        setSize(475, 265);
        setLocationRelativeTo(getOwner());
        //GEN-       remove END:initComponents
    }

    private void btnClean(final ActionEvent e) {
        txtVysledek.setText("");
    }

    //GEN-BEGIN:variables
    private JPanel results;
    private JLabel txtVysledek;
    private JPanel calculatorButtons;
    private JButton btnRovnost;
    private JButton clean;
    private JPanel numbers;
    private JButton btnCislo1;
    private JButton btnCislo2;
    private JButton btnCislo3;
    private JButton btnCislo4;
    private JButton btnCislo5;
    private JButton btnCislo6;
    private JButton btnCislo7;
    private JButton btnCislo8;
    private JButton btnCislo9;
    private JButton btnCislo0;
    private JPanel operators;
    private JButton btnPlus;
    private JButton btnMinus;
    private JButton btnKrat;
    //GEN- remove  END:variables
}
