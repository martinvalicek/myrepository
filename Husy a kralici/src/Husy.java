import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Husy {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in, StandardCharsets.UTF_8);
        int cislo1, cislo2;

        System.out.println("Kalkulačka");

        System.out.print("Zadej první číslo");
        cislo1 = Integer.parseInt(s.nextLine());

        System.out.print("Zadej druhé číslo");
        cislo2 = Integer.parseInt(s.nextLine());

        System.out.println(cislo1 + " + " + cislo2 + " = " + (cislo1 + cislo2));
        System.out.println(cislo1 + " - " + cislo2 + " = " + (cislo1 - cislo2));
        System.out.println(cislo1 + " * " + cislo2 + " = " + (cislo1 * cislo2));
        System.out.println(cislo1 + " / " + cislo2 + " = " + ((float)cislo1 / cislo2));
    }
}