import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Lekar extends JFrame {
    public Lekar() {
        initComponents();
    }

    private void btnVypocetActionPerformed(ActionEvent e) {
        String actualText1 = txtCas.getText();
        String actualText2 = txtNavstevy.getText();
    }

    private void initComponents() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        lblNadpis = new JLabel();
        lblCas = new JLabel();
        lblNavstevy = new JLabel();
        txtCas = new JTextField();
        txtNavstevy = new JTextField();
        btnVypocet = new JButton();
        lblDny = new JLabel();
        lblUtrata = new JLabel();
        lblPrumerneDni = new JLabel();
        lblUtrati = new JLabel();

        //okno
        setTitle("Cas straveny u lekare");
        setBackground(Color.black);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //lblNadpis
        lblNadpis.setText("Prumerna doba stravena duchodcem u lekaru");
        lblNadpis.setFont(new Font("Segoe UI Black", Font.PLAIN, 20));
        lblNadpis.setForeground(Color.red);
        contentPane.add(lblNadpis);
        lblNadpis.setBounds(5, 5, 455, lblNadpis.getPreferredSize().height);

        //lblCas
        lblCas.setText("Cas u lekare:");
        lblCas.setFont(new Font(Font.SERIF, Font.PLAIN, 20));
        lblCas.setForeground(Color.yellow);
        contentPane.add(lblCas);
        lblCas.setBounds(new Rectangle(new Point(5, 50), lblCas.getPreferredSize()));

        //lblNavstevy
        lblNavstevy.setText("Pocet navstev za tyden:");
        lblNavstevy.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        lblNavstevy.setForeground(Color.yellow);
        contentPane.add(lblNavstevy);
        lblNavstevy.setBounds(new Rectangle(new Point(5, 80), lblNavstevy.getPreferredSize()));

        //txtCas
        txtCas.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        contentPane.add(txtCas);
        txtCas.setBounds(115, 45, 150, txtCas.getPreferredSize().height);

        //txtNavstevy
        txtNavstevy.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        contentPane.add(txtNavstevy);
        txtNavstevy.setBounds(210, 80, 150, txtNavstevy.getPreferredSize().height);

        //btnVypocet
        btnVypocet.setText("Vypocti");
        btnVypocet.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        btnVypocet.addActionListener(e -> btnVypocetActionPerformed(e));
        contentPane.add(btnVypocet);
        btnVypocet.setBounds(90, 120, 235, btnVypocet.getPreferredSize().height);

        //lblDny
        lblDny.setText("Prumerne dni:");
        lblDny.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        contentPane.add(lblDny);
        lblDny.setBounds(new Rectangle(new Point(5, 165), lblDny.getPreferredSize()));

        //lblUtrata
        lblUtrata.setText("Utrati:");
        lblUtrata.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        contentPane.add(lblUtrata);
        lblUtrata.setBounds(new Rectangle(new Point(5, 195), lblUtrata.getPreferredSize()));

        //lblPrumerneDni
        lblPrumerneDni.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        lblPrumerneDni.setText("0");
        contentPane.add(lblPrumerneDni);
        lblPrumerneDni.setBounds(130, 165, 100, lblPrumerneDni.getPreferredSize().height);

        //lblUtrati
        lblUtrati.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        lblUtrati.setText("0");
        contentPane.add(lblUtrati);
        lblUtrati.setBounds(60, 195, 105, lblUtrati.getPreferredSize().height);

        {
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());
    }

    private JLabel lblNadpis;
    private JLabel lblCas;
    private JLabel lblNavstevy;
    private JTextField txtCas;
    private JTextField txtNavstevy;
    private JButton btnVypocet;
    private JLabel lblDny;
    private JLabel lblUtrata;
    private JLabel lblPrumerneDni;
    private JLabel lblUtrati;
}