public class du {
    import java.nio.charset.StandardCharsets;
import java.util.Scanner;

    public class diagram1main {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in, StandardCharsets.UTF_8);
            int cislo = -1;
            int pocetKladnych = 0;
            int pocetNulovych = 0;
            int pocetZapornych = 0;
            int soucetZapornych = 0;

            System.out.print("Zadej číslo");
            cislo = Integer.parseInt(s.nextLine());
            while (cislo != -999) {
                if (cislo > 0) {
                    pocetKladnych++;
                }
                if (cislo == 0) {
                    pocetNulovych++;
                }
                if (cislo < 0) {
                    pocetZapornych++;
                    soucetZapornych = soucetZapornych + cislo;
                }
                System.out.print("Zadej číslo");
                cislo = Integer.parseInt(s.nextLine());
            }
            System.out.println("Počet kladných: " + pocetKladnych);
            System.out.println("Počet nulových: " + pocetNulovych);
            if (pocetZapornych > 0) {
                System.out.println("Průměr záporných: " + (soucetZapornych) / (float) pocetZapornych);
            }
        }
    }
}
