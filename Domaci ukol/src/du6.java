public class du6 {
    import java.nio.charset.StandardCharsets;
import java.util.Scanner;

    public class diagram6main {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in, StandardCharsets.UTF_8);
            int cislo = -1;
            int poradiNejmensi = 0;
            int poradi = 0;
            int nejmensi;

            System.out.print("Zadej číslo");
            cislo = Integer.parseInt(s.nextLine());
            nejmensi = cislo;

            while (cislo != -999) {
                poradi++;
                if (cislo < nejmensi) {
                    nejmensi = cislo;
                    poradiNejmensi = poradi;
                }
                System.out.print("Zadej číslo");
                cislo = Integer.parseInt(s.nextLine());
            }

            System.out.println("Nejmenší: " + nejmensi);
        }
    }

}
