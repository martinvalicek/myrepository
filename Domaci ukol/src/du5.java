public class du5 {
    import java.nio.charset.StandardCharsets;
import java.util.Scanner;

    public class diagram5main {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in, StandardCharsets.UTF_8);
            int cislo = -1;
            int prvni = -1;

            System.out.print("Zadej číslo");
            prvni = Integer.parseInt(s.nextLine());
            System.out.println(prvni);

            while (cislo != -999) {
                System.out.print("Zadej číslo");
                cislo = Integer.parseInt(s.nextLine());

                if (cislo % 2 == 0 && prvni % 2 == 0) {
                    System.out.println(cislo);
                }
                if (cislo % 2 == 1 && prvni % 2 == 1) {
                    System.out.println(cislo);
                }
            }
        }
    }
}
