public class du2 {
    import java.nio.charset.StandardCharsets;
import java.util.Scanner;

    public class diagram2main {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in, StandardCharsets.UTF_8);
            int cislo = -1;
            int nejvetsi = -1;
            int pocetLichych = 0;
            int soucetLichych = 0;

            System.out.print("Zadej číslo");
            cislo = Integer.parseInt(s.nextLine());
            while (cislo != -999) {
                if (cislo % 2 == 0) {
                    if (cislo > nejvetsi) {
                        nejvetsi = cislo;
                    }
                }
                else {
                    pocetLichych++;
                    soucetLichych = soucetLichych + cislo;
                }
                System.out.print("Zadej číslo");
                cislo = Integer.parseInt(s.nextLine());
            }

            System.out.println("Největší sudé číslo: " + nejvetsi);
            if (pocetLichych > 0) {
                System.out.println("Průměr lichých: " + (soucetLichych / (float) pocetLichych));
            }
        }
    }
}
