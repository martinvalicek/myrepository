import java.awt.*;
import javax.swing.*;
/*
 * Created by JFormDesigner on Thu Feb 13 09:13:39 CET 2020
 */



/**
 * @author Martin Val쎭쒍ek
 */
public class GameWindow extends JFrame {
    public GameWindow() {
        initComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Martin Val쎭쒍ek
        progressBar1 = new JProgressBar();

        //======== this ========
        setMinimumSize(new Dimension(800, 500));
        setTitle("Hra");
        setBackground(Color.magenta);
        var contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(progressBar1);
        progressBar1.setBounds(150, 165, 210, 65);

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(800, 500);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Martin Val쎭쒍ek
    private JProgressBar progressBar1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
